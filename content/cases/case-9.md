+++
date = 2020-03-27T15:15:03Z
e-number-default-0-explicit-set = nil
e-number-default-set = 5
e-number-no-default-set = nil
integer-default-explicit-set-to-zero = 0
integer-no-default = nil
integer-with-default = 5
title = "Case 9"
[field-group-case]
field-group-number-0-default-explicit-set = 0
field-group-number-default-set = 5
field-group-number-no-default-set = nil
[field-group-included-templates]
e-number-default-0-explicit-set = nil
e-number-default-set = "5"
e-number-no-default-set = nil
[[repeatable-field-group]]
integer-default-explicit-set-to-zero = 0
integer-no-default = nil
integer-with-default = 5
[[repeatable-field-group]]
integer-default-explicit-set-to-zero = 0
integer-no-default = nil
integer-with-default = 5

+++
Testing repeatable field group. 