+++
date = 2020-03-26T14:07:44Z
integer-no-default = 0
integer-with-default = 5
title = "Test 3"

+++
Entry created from Forestry with field "integer no default" in FMT displaying 0 (not explicitly by editor) and left blank in the entry. 

TOML format demonstrates Forestry saves with nil value. Hugo 0.63.2 does not serve this, reporting: 

    Error: Error building site: "/Users/nick/Projects/hugo-forestry-zero-values-tests/content/cases/test-3.md:3:1": unmarshal failed: Near line 2 (last key parsed 'integer-no-default'): expected value but found "nil" instead