+++
date = 2020-03-26T17:58:17Z
e-number-default-set = 5
e-number-no-default-set = 12
integer-default-explicit-set-to-zero = 0
integer-no-default = 12
integer-with-default = 5
title = "Test 6"
[field-group-case]
field-group-number-0-default-explicit-set = 0
field-group-number-default-set = 5
field-group-number-no-default-set = 12

+++
This case includes number fields, added to field groups.