+++
date = 2020-03-26T18:14:22Z
e-number-default-0-explicit-set = 14
e-number-default-set = 5
e-number-no-default-set = 14
integer-default-explicit-set-to-zero = 0
integer-no-default = 14
integer-with-default = 5
title = "Case 8"
[field-group-case]
field-group-number-0-default-explicit-set = 0
field-group-number-default-set = 5
field-group-number-no-default-set = 14
[field-group-included-templates]
e-number-default-0-explicit-set = 14
e-number-default-set = "5"
e-number-no-default-set = 14

+++
Behold, from Forestry.