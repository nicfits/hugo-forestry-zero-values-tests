+++
date = 2020-04-07T15:50:07Z
e-number-default-0-explicit-set = 7
e-number-default-set = 5
e-number-no-default-set = 0
e-string-default-set = "Hello"
e-string-no-default-set = ""
integer-default-explicit-set-to-zero = 0
integer-no-default = 0
integer-with-default = 5
repeatable-field-group = []
string-default-set = "Hello"
string-no-default = ""
string_blocks = []
title = "case 13 - April 7 updated with blocks"
[field-group-case]
field-group-number-0-default-explicit-set = 0
field-group-number-default-set = 5
field-group-number-no-default-set = 0
[field-group-included-templates]
e-number-default-0-explicit-set = "7"
e-number-default-set = "5"
e-number-no-default-set = "0"
[[number_blocks]]
integer-default-explicit-set-to-zero = 0
integer-no-default = 0
integer-with-default = 5
template = "b-direct-number-fields"
[[number_blocks]]
e-number-default-0-explicit-set = 7
e-number-default-set = 5
e-number-no-default-set = 0
template = "b-included-template-number-fields"

+++
