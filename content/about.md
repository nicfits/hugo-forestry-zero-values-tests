+++
date = 2020-03-26T13:41:46Z
title = "readme"
[menu.main]

+++
This Hugo site helps us to test Forestry.io's zero and null value handling in front matter fields, and to develop best practices in Hugo for handling them.

Currently (3/26/2020) Forestry incorrectly handles default 0 values in number fields, leaving them as nil. You can see this by creating a new Case content object in Forestry. Hugo will fail to build, so to continue work you'll need to manually update the Forestry .md file.

## Findings

### Number fields in FMT

* fail: number field in FMT with default accepted as is (displays 0) incorrectly sets front matter to nil
* Note: number field with default explicitly set to 0 or other integer works as expected

### Number fields in field groups in FMT

* fail: number field in field group FMT with default accepted as is (displays 0) incorrectly sets front matter to nil

### Number fields in repeatable field groups in FMT

* fail: number field in field group FMT with default accepted as is (displays 0) incorrectly sets front matter to nil

### Number fields in Blocks

* fail: number field in field group FMT with default accepted as is (displays 0) incorrectly sets front matter to nil

### Number fields in included FMT templates

* Fail: number field added to FMT as included template, with default accepted as is (displays 0) incorrect sets front matter to nil.
* Fail: number field added to FMT as included template, with default explicitly set to 0, fails, sets to nil. 

### Number fields, in included templates, in FMT field groups

* Fail: number field added to FMT as included template in a field group, with default accepted as is (displays 0) incorrect sets front matter to nil.
* Fail: number field added to FMT as included template in a field group, with default explicitly set to 0, fails, sets to nil. 

### Number fields, in included templates, in Blocks

* Fail: number field added to FMT as included template in a field group, with default accepted as is (displays 0) incorrect sets front matter to nil.
* Fail: number field added to FMT as included template in a field group, with default explicitly set to 0, fails, sets to nil.
